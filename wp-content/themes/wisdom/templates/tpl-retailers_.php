<?php
	/* Template name: Retailers */
	get_header('shop');
	
	the_post();
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
		<div class="faqSection">
			<div class="faqGrid">
				<div class="faqGridWrapper">
					<div class="faqGridHeader">
						<h5>Browse</h5>
						<h2>Online Retailers</h2>
						<img src="<?php echo get_bloginfo('template_url'); ?>/pics/2-pulls.png">
					</div>
					<div class="faqGridContainer">
						<div class="faqGridTable">
							<ul>
								<li>Why is pricing not shown on your website?</li>
								<li>How do I install my product?</li>
								<li>Is there a warranty on my product?</li>
								<li>What do I need to provide for a warranty issue?</li>
								<li>Where do I find my invoice and purchase order number?</li>
								<li>Are key blanks available in-store?</li>
								<li>How can I customize my monogram or emblem?</li>
								<li>Can I buy your products directly from you?</li>
								<li>Are replacement parts available?</li>
							</ul>

							<p>Can’t find what you’re looking for?</p>
							<a href="">
 								<button>Contact Us</button>
 							</a>
						</div>

						<div class="faqGridContent">
							<div class="faqReveal">
								<h4>Why is pricing not shown on your website?</h4>
								<p>Unfortunately, our pricing information is not listed on our website because we do not sell direct. To find retail pricing, please visit one of the many options for our official retailers on the Where to Buy page.</p>
							</div>
							<div class="faqReveal">
								<h4>How do I install my product?</h4>
								<p>We provide full assembly instructions for each of our products. You can find these instructions on each product page of our website.  Please refer to the product(s) you have purchased to download the instructions.</p>
							</div>
							<div class="faqReveal">
								<h4>Is there a warranty on my product?</h4>
								<p>Architectural Mailboxes guarantees their products against defects in workmanship and materials, under normal use and proper assembly, for a period of 1 year from date of purchase. Our complete User Warranty can be found here.</p>
							</div>
							<div class="faqReveal">
								<h4>What do I need to provide for a warranty issue?</h4>
								<p>We guarantee our products against defects in workmanship and materials. If you are experiencing an issue with one of our products, please contact our Customer Experience Department for more assistance. You will need proof of purchase and all details about the issue you are experiencing.</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ -->
</main>
<?php
	
	get_footer();
?>