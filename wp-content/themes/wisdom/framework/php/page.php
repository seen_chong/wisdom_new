<?php
	add_action('admin_menu', 'page_add_custom_box');
	add_action('save_post', 'page_save_postdata');

	function page_add_custom_box() {
		$server_url = $GLOBALS['HTTP_SERVER_VARS']['REQUEST_URI'];
			
		if (function_exists('add_meta_box')) {
			if ($_GET['post'] == '2') {
				add_meta_box('page_extra_options1','Header Elements', 'page_extra_options1', 'page', 'normal', 'high');
			}
			if ($_GET['post'] == '18') {
				add_meta_box('page_extra_options2','Contact Elements', 'page_extra_options2', 'page', 'normal', 'high');
			}
		}
	}
	
	function page_extra_options1($post_id) {
		$header_text1 = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'header_text1', true)));
		$header_link1 = get_post_meta($post_id->ID, 'header_link1', true);
		$header_text2 = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'header_text2', true)));
		$header_link2 = get_post_meta($post_id->ID, 'header_link2', true);
?>
		<input type="hidden" name="cms_product_noncename" id="cms_product_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
		<p><strong>1st Button</strong></p>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="50%">Text</td>
				<td style="padding-left: 12px;">Link</td>
			</tr>
			<tr>
				<td width="50%">
					<input type="text" style="width:100%" name="header_text1" value="<?php echo $header_text1; ?>" />
				</td>
				<td style="padding-left: 12px;">
					<input type="text" style="width:100%" name="header_link1" value="<?php echo $header_link1; ?>" />
				</td>
			</tr>
		</table>
		<p><strong>2nd Button</strong></p>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="50%">Text</td>
				<td style="padding-left: 12px;">Link</td>
			</tr>
			<tr>
				<td width="50%">
					<input type="text" style="width:100%" name="header_text2" value="<?php echo $header_text2; ?>" />
				</td>
				<td style="padding-left: 12px;">
					<input type="text" style="width:100%" name="header_link2" value="<?php echo $header_link2; ?>" />
				</td>
			</tr>
		</table>
		<p><strong>Slider</strong></p>
<?php
		$slider = get_post_meta($post_id->ID, 'slider', true);
?>
		<p><input type="button" class="button button-large add-slide button-primary" value="Add Slide"/></p>
		<div class="list-elements">
<?php
			if (is_array($slider)) {
				for ($i = 0; $i < count($slider['image']); $i++) {
					$id = $slider['image'][$i];
					$image = wp_get_attachment_image_src($id, 'full');
?>
					<div class="rounded-div" style="padding-bottom: 10px;">
						<div>
							<input name="slider_slide[]" type="text" style="width: 450px" value="<?php echo $image[0]; ?>">
							<input type="button" class="button button-primary upload-image" value="Upload image">
							<input type="button" class="button remove-section" value="Remove">
							<input name="slide[image][]" type="hidden" value="<?php echo $id; ?>">
						</div>
					</div>
<?php					
				}
			}
?>
		</div>
<?php
	}
	
	function page_extra_options2($post_id) {
		$contact_field1 = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'contact_field1', true)));
		$contact_field2 = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'contact_field2', true)));
		$contact_field3 = htmlspecialchars(stripslashes(get_post_meta($post_id->ID, 'contact_field3', true)));
?>
		<input type="hidden" name="cms_product_noncename" id="cms_product_noncename" value="<?php echo wp_create_nonce(plugin_basename(__FILE__)); ?>" />
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="33.3%">Address</td>
				<td width="33.3%" style="padding-left: 12px;">Phone</td>
				<td style="padding-left: 12px;">Fax</td>
			</tr>
			<tr>
				<td width="33.3%">
					<input type="text" style="width:100%" name="contact_field1" value="<?php echo $contact_field1; ?>" />
				</td>
				<td width="33.3%" style="padding-left: 12px;">
					<input type="text" style="width:100%" name="contact_field2" value="<?php echo $contact_field2; ?>" />
				</td>
				<td style="padding-left: 12px;">
					<input type="text" style="width:100%" name="contact_field3" value="<?php echo $contact_field3; ?>" />
				</td>
			</tr>
		</table>
<?php
	}

	function page_save_postdata($post_id) {
		if ( !wp_verify_nonce( $_POST['cms_product_noncename'], plugin_basename(__FILE__) )) {
			return $post_id;
		}
			
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post_id;
			
		if ($_POST['post_type']=='page') {
			$header_text1 = $_POST['header_text1'];
			$header_link1 = $_POST['header_link1'];
			$header_text2 = $_POST['header_text2'];
			$header_link2 = $_POST['header_link2'];
			
			if ($header_text1 != '') 
				update_post_meta($post_id, 'header_text1', $header_text1);
			else
				delete_post_meta($post_id, 'header_text1');
			
			if ($header_link1 != '') 
				update_post_meta($post_id, 'header_link1', $header_link1);
			else
				delete_post_meta($post_id, 'header_link1');
			
			if ($header_text2 != '') 
				update_post_meta($post_id, 'header_text2', $header_text2);
			else
				delete_post_meta($post_id, 'header_text2');
			
			if ($header_link2 != '') 
				update_post_meta($post_id, 'header_link2', $header_link2);
			else
				delete_post_meta($post_id, 'header_link2');
			
			$slide = $_POST['slide'];
			
			if (count($slide) > 0) 
				update_post_meta($post_id, 'slider', $slide);
			else
				delete_post_meta($post_id, 'slider');
			
			$contact_field1 = $_POST['contact_field1'];
			$contact_field2 = $_POST['contact_field2'];
			$contact_field3 = $_POST['contact_field3'];
			
			if ($contact_field1 != '') 
				update_post_meta($post_id, 'contact_field1', $contact_field1);
			else
				delete_post_meta($post_id, 'contact_field1');
			
			if ($contact_field2 != '') 
				update_post_meta($post_id, 'contact_field2', $contact_field2);
			else
				delete_post_meta($post_id, 'contact_field2');
			
			if ($contact_field3 != '') 
				update_post_meta($post_id, 'contact_field3', $contact_field3);
			else
				delete_post_meta($post_id, 'contact_field3');
		}
	}
?>