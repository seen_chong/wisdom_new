<?php
	get_header();
	
	the_post();
	
	$product_short = get_post_meta(get_the_ID(), 'product_short', true);
	$width  = get_post_meta(get_the_ID(), 'width', true);
	$height = get_post_meta(get_the_ID(), 'height', true);
	$length = get_post_meta(get_the_ID(), 'length', true);
	$product_vars = get_post_meta(get_the_ID(), 'product_vars', true);
	$product_vars = $product_vars['name'];
	
	$first_gallery = $product_vars[0]['colors'][0]['images'];
	$first_crystal = $product_vars[0]['colors'][0]['title'];
        $first_description = $product_vars[0]['colors'][0]['description'];
	
	$feat_image    = $first_gallery[0];
	$feat_image    = wp_get_attachment_image_src($feat_image, 'full');
	
	$category      = get_the_terms(get_the_ID(), 'product_category');
	$category	  = $category[0]->term_id;
?>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ -->
<main role="main"> 
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<div class="section">
		<div class="productHero" style="background-image:url(<?php the_field('header_photo'); ?>);">

</div>
		<div class="container wow fadeIn" data-wow-delay="0.4s">
			<article id="product" class="clearfix">
							<nav class="woocommerce-breadcrumb"><a href="/shop">Products</a> / <?php the_title(); ?></nav>
				<div class="images">
					<a class="unfancybox" href="<?php echo $feat_image[0]; ?>">
						<img class="large img-responsive aligncenter" src="<?php echo $feat_image[0]; ?>" width="599" height="367" alt=" ">
					</a>
					<div class="owl-carousel">
					<?php
						foreach ($first_gallery as $image) {
							$image_src = wp_get_attachment_image_src($image, 'full');
					?>
							<a class="thumb" href="<?php echo $image_src[0]; ?>">
								<img class="img-responsive" src="<?php echo $image_src[0]; ?>" width="133" height="134" alt=" ">
							</a>
					<?php		
						}
					?>
					</div>
				</div>

				<div class="details">
					<h1><?php the_title(); ?></h1>
					<?php echo wpautop($product_short); ?>
					<div class="product_share">
						<?php echo do_shortcode( '[addtoany]' ); ?>
					</div>
					<div class="options">
<!-- 						<div class="col">
							<div class="label">Color:</div>
						</div> -->
						<div class="col">
							<div class="select-box">
								<span class="active">Finish</span>
								<select style="opacity: 0;" id="polish-values">
									<option value="0">Finish</option>
<?php
								$count_vars = 0;
								
								foreach ($product_vars as $product_var) {
?>
									<option value="<?php echo $count_vars; ?>"><?php echo $product_var['title']; ?></option>
<?php									
									$count_vars++;
								}
?>
								</select>
							</div>
						</div>
<?php
						if ($first_crystal == '') {
							$crystals_block_css  = ' style="display: none"';	
							$crystals_block_css1 = ' display: none';	
						}
?>
						<div class="col"<?php echo $crystals_block_css ?>>
							<div class="select-box"<?php echo $crystals_block_css ?>>
								<span class="active">Crystal Colors</span>
								<select style="opacity: 0;<?php echo $crystals_block_css1; ?>" id="color-values"></select>
							</div>
						</div>
                                                <?php if(count($product->get_variation_attributes()['pa_size']) > 0) { ?>
						<div class="col">
							<div class="select-box">
								<span class="active">Size</span>
								<select style="opacity: 0;" id="size-options">
									<?php foreach($product->get_variation_attributes()['pa_size'] as $size) { ?>
									<option value=""><?php echo $size; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<?php } ?>
						<div id="reset">
							<p><u>Reset</u></p>
						</div>
						<script type="text/javascript">
							$("#reset").click(function(){
							  document.location.reload(true);
							});
						</script>
						<textarea style="display: none" id="variations"><?php echo json_encode($product_vars); ?></textarea>
						<input type="hidden" id="product_id" value="<?php echo get_the_ID(); ?>" />
					</div>
					<div class="dimensions">
						<div class="col">
							<span class="label">Width:</span> <?php echo $width; ?> </div>
						<div class="col">
							<span class="label">Height:</span> <?php echo $height; ?> </div>
						<div class="col">
							<span class="label">Length:</span> <?php echo $length; ?> </div>
					</div>
                    <div id="default-description" style="display: none">
						<?php echo the_content();?>
					</div>
					<div class="info" id="color-description">
						<!-- <h4>Product Info:</h4> -->
						<?php echo $first_description?:the_content(); ?>
					</div>
					<div class="drawings">
						<?php echo do_shortcode("[my-gallery]"); ?>
					</div>

					<div class="request_info">
						<a href="/contact">
							<button type="submit" class="single_add_to_cart_button button alt">Request More Info</button>
						</a>
					</div>
				</div>
			</article>
		</div>
	</div>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ --> 
	
	<!-- ^^^^^^^^^^^^^^^^^  *SECTION  ^^^^^^^^^^^^^^^^^ -->
	<aside class="section" style="padding-top:0;">


			<div class="container wow fadeIn" data-wow-delay="0.4s">
				<h2 class="main-title">You may also like...</h2>
				<div class="products-entries clearfix">
					<?php echo do_shortcode('[related_products per_page="4"]'); ?>
					<!-- --> 
					<!-- --> 

				</div>
			</div>

	</aside>
	<!-- ^^^^^^^^^^^^^^^^^  * end SECTION  ^^^^^^^^^^^^^^^^^ --> 
</main>
<script>
	var descriptions = <?php echo json_encode($product_vars);?>;
	var colorOption = false;
	<?php if($first_crystal != ''){ ?>
	colorOption = true;
	<?php  } ?>
	jQuery('#polish-values').on('change', function(){
		var value = $(this).val();
		if(colorOption){
			var html = descriptions[value]['colors'][0]['description'];
			if(!html || html == ''){
                            html = jQuery('#default-description').html();
			}
			jQuery('#color-description').html(html);
		}
	});

	if(colorOption){
		jQuery('#color-values').on('change', function(){
			var color = $(this).val();
			var value = jQuery('#polish-values').val();
			var html = descriptions[value]['colors'][color]['description'];
			if(!html || html == ''){
			    html = jQuery('#default-description').html();
			}
			jQuery('#color-description').html(html);
		});
	}


</script>

<script type="text/javascript">
// $('div.drawings div a.zoom').attr('target', '_blank').attr('title','This link will open in a new window.');

</script>
								<script type="text/javascript">
				$("a[href$='.jpg'],a[href$='.png'],a[href$='.gif']").attr('rel', 'gallery').fancybox();

				</script>

								<script type="text/javascript">
				$( "a.thumb" ).click(function() {
					  $( ".fancybox-skin" ).hide();
					});

				</script>
<!-- §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ 
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     *end MAIN CONTENT
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	 §§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§ --> 
<?php
	
	get_footer();
?>
